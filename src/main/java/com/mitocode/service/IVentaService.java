package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Venta;

public interface IVentaService {

	Venta registrar(Venta Venta);
	void modificar(Venta Venta);
	void eliminar(int idVenta);
	Venta listarId(int idVenta);
	List<Venta> listar();
}
