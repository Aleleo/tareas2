package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Venta;
import com.mitocode.service.IVentaService;

@RestController
@RequestMapping("/venta")
public class VentaController {

	@Autowired
	private IVentaService service;
	
	@GetMapping(value ="/listar", produces =  MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Venta>> listar(){
		List<Venta> Venta = new ArrayList<>();
		try {
			Venta = service.listar();
		} catch (Exception e) {
			new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Venta>>(Venta, HttpStatus.OK);
	}
	
	@PostMapping(value ="/registrar",consumes = MediaType.APPLICATION_JSON_VALUE, 
				 produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Venta> registrar(@RequestBody Venta Venta) {
		Venta consult = new Venta();
		try {
			consult = service.registrar(Venta);
		}catch(Exception e) {
			new ResponseEntity<Venta>(consult,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Venta>(consult,HttpStatus.OK);
	}
	

}
