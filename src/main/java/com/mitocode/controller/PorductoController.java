package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Producto;
import com.mitocode.service.IProductoService;


@RestController
@RequestMapping("/producto")
public class PorductoController {

	
	@Autowired
	private IProductoService service;
	
	@GetMapping(value ="/listar", produces =  MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producto>> listar(){
		List<Producto> medico = new ArrayList<>();
		try {
			medico = service.listar();
		} catch (Exception e) {
			new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Producto>>(medico, HttpStatus.OK);
	}
	
	@GetMapping(value ="/listar/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> listarId(@PathVariable("id") Integer id) {
		Producto medico = new Producto();
		try {
			medico = service.listarId(id);
		}catch(Exception e) {
			new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity <Producto>(medico,HttpStatus.OK);
	}
	
	@PostMapping(value ="/registrar",consumes = MediaType.APPLICATION_JSON_VALUE, 
				 produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Producto> registrar(@RequestBody Producto medico) {
		Producto med = new Producto();
		try {
			med = service.registrar(medico);
		}catch(Exception e) {
			new ResponseEntity<Producto>(med,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Producto>(med,HttpStatus.OK);
	}
	
	@PutMapping(value ="/actualizar",consumes = MediaType.APPLICATION_JSON_VALUE, 
			    produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Integer> actualizar(@RequestBody Producto medico) {
		int resultado = 0;
		try {
			service.modificar(medico);
			resultado = 1;
		}catch(Exception e) {
			new ResponseEntity<Integer>(resultado,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity <Integer>(resultado,HttpStatus.OK);
	}
	
	@DeleteMapping(value ="/eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Integer> eliminar(@PathVariable Integer id) {
		int resultado = 0;
		try {
			service.eliminar(id);
			resultado = 1;
		}catch(Exception e) {
			new ResponseEntity<Integer>(resultado,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity <Integer>(resultado,HttpStatus.OK);
	}
}
