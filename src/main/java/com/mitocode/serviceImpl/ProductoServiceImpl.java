package com.mitocode.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IProductoDAO;
import com.mitocode.model.Producto;
import com.mitocode.service.IProductoService;

@Service
public class ProductoServiceImpl implements IProductoService{

	@Autowired
	private IProductoDAO dao;
	
	@Override
	public Producto registrar(Producto pro) {
		return dao.save(pro);
	}

	@Override
	public void modificar(Producto pro) {
		dao.save(pro);
	}

	@Override
	public void eliminar(int idProducto) {
		dao.delete(idProducto);
	}

	@Override
	public Producto listarId(int idProducto) {
		return dao.findOne(idProducto);
	}

	@Override
	public List<Producto> listar() {
		return dao.findAll();
	}

}
