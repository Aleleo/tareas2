package com.mitocode.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IPersonaDAO;
import com.mitocode.model.Persona;
import com.mitocode.service.IPersonaService;

@Service
public class PersonaServiceImpl implements IPersonaService{

	@Autowired
	private IPersonaDAO dao;
	
	@Override
	public Persona registrar(Persona per) {
		return dao.save(per);
	}

	@Override
	public void modificar(Persona per) {
		dao.save(per);
	}

	@Override
	public void eliminar(int idPersona) {
		dao.delete(idPersona);
	}

	@Override
	public Persona listarId(int idPersona) {
		return dao.findOne(idPersona);
	}

	@Override
	public List<Persona> listar() {
		return dao.findAll();
	}

}
